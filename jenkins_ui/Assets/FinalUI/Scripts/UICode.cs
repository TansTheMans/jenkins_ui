﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICode : MonoBehaviour
{
    public Animator infoButton;
    public Animator musicButton;
    public Animator infoDialogue;
    public Animator musicDialogue;
    public Animator brContent;
    public Animator tlContent;

    public void OpenInfo()
    {
        infoButton.SetBool("isHidden", true);
        musicButton.SetBool("isHidden", true);
        infoDialogue.SetBool("isHidden", false);
    }

    public void OpenMusic()
    {
        infoButton.SetBool("isHidden", true);
        musicButton.SetBool("isHidden", true);
        musicDialogue.SetBool("isHidden", false);
    }

    public void CloseInfo()
    {
        infoButton.SetBool("isHidden", false);
        musicButton.SetBool("isHidden", false);
        infoDialogue.SetBool("isHidden", true);
    }

    public void CloseMusic()
    {
        infoButton.SetBool("isHidden", false);
        musicButton.SetBool("isHidden", false);
        musicDialogue.SetBool("isHidden", true);
    }

    public void TLToggleMenu()
    {
        bool isHidden = tlContent.GetBool("isHidden");
        tlContent.SetBool("isHidden", !isHidden);
    }

    public void BRToggleMenu()
    {
        bool isHidden = brContent.GetBool("isHidden");
        brContent.SetBool("isHidden", !isHidden);
    }
}
